document
  .getElementById("dark-mode-toggle")
  .addEventListener("click", function () {
    document.body.classList.toggle("dark");
    const textElements = document.querySelectorAll(
      "input, textarea, h1, h2, div",
    );
    textElements.forEach((element) =>
      element.classList.toggle("dark-mode-text"),
    );
  });

document.getElementById("folder-submit").addEventListener("click", function () {
  var folderInput = document.getElementById("folder-input");
  var folder = folderInput.value;
  folderInput.value = "";

  var folderSelect = document.getElementById("folder-select");
  var option = document.createElement("option");
  option.text = folder;
  folderSelect.add(option);
});

var recognition = new (window.SpeechRecognition ||
  window.webkitSpeechRecognition ||
  window.mozSpeechRecognition ||
  window.msSpeechRecognition)();
recognition.lang = "en-US";
recognition.interimResults = false;
recognition.maxAlternatives = 1;

var recognizing = false;

document.getElementById("voice-input").addEventListener("click", function () {
  var voiceButton = document.getElementById("voice-input");
  if (recognizing) {
    recognition.stop();
    voiceButton.textContent = "Start Voice Input";
    voiceButton.classList.remove("bg-red-500");
    voiceButton.classList.add("bg-green-500");
    recognizing = false;
  } else {
    recognition.start();
    voiceButton.textContent = "Stop Voice Input";
    voiceButton.classList.remove("bg-green-500");
    voiceButton.classList.add("bg-red-500");
    recognizing = true;
  }
});

recognition.onresult = function (event) {
  var noteInput = document.getElementById("note-input");
  noteInput.value = event.results[0][0].transcript;
};

// Removed redundant event listener

// Removed image, video, and audio upload handlers

// Removed redundant event listener

  var timestamp = document.createElement("div");
  timestamp.classList.add("text-sm", "text-gray-500");
  timestamp.textContent = new Date().toLocaleString();
  noteElement.appendChild(timestamp);

  var folderElement = document.getElementById(folder);
  if (!folderElement) {
    folderElement = document.createElement("div");
    folderElement.id = folder;
    folderElement.classList.add(
      "folder",
      "mb-10",
      "bg-white",
      "shadow-md",
      "rounded-lg",
      "p-5",
    );
    folderElement.innerHTML = `<h2 class="text-2xl text-gray-800 mb-5">${folder}</h2>`;
    document.getElementById("notes").appendChild(folderElement);
  }
  folderElement.appendChild(noteElement);
});
